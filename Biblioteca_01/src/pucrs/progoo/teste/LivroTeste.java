package pucrs.progoo.teste;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pucrs.progoo.Livro;

public class LivroTeste {
	private Livro livroTeste;

	@Before
	public void setUp() throws Exception {
		livroTeste = new Livro(1,"lord of the rings","jrr.tolkien","hobbit",1978,1);
	}

	@Test
	public void emprestarTeste() {
		double esperado = 1;
		
		double atual =(double)livroTeste.getTotal();
		assertEquals(esperado, atual, 0.1);
	}

}
