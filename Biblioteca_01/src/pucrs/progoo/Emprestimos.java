package pucrs.progoo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Emprestimos {

	private ArrayList<Emprestimo> emprestimos;

	public Emprestimos() {
		emprestimos = new ArrayList<>();
	}
	
	public void criar(Usuario usuario, Livro livro, LocalDate dataLimite)
	{
		Emprestimo emp = new Emprestimo(usuario,livro,dataLimite);
		emprestimos.add(emp);
	}
	
	public void listarTodos() {
		for(Emprestimo emp: emprestimos)
			System.out.println(emp);
	}
	
	public ArrayList<Emprestimo> buscarPorUsuario(Usuario usu)
	{
		ArrayList<Emprestimo> result = new ArrayList<>();
		for(Emprestimo emp: emprestimos)
			if(emp.getUsuario().getCodigo().equals(usu.getCodigo()))
				result.add(emp);
		return result;
	}
	public void ordenaData() {
        Collections.sort(emprestimos);
    }
	public void ordenaDataLimite() {       
        emprestimos.sort(Comparator.comparing(Emprestimo::getDataLimite));
    }
	
}
