package pucrs.progoo;

public class Livro implements Emprestavel, Comparable<Livro> {

	private int codigo;
	private String titulo;
	private String autor;
	private String editora;
	private int ano;
	private int total;

	public Livro(int codigo, String titulo, String autor, String editora, int ano, int total) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.autor = autor;
		this.editora = editora;
		this.ano = ano;
		this.total = total;
	}

	public Livro(int codigo, String titulo, String autor, String editora, int ano) {
		this(codigo,titulo,autor,editora,ano,1);
	}

	public int getCodigo() {
		return codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getAutor() {
		return autor;
	}

	public String getEditora() {
		return editora;
	}

	public int getAno() {
		return ano;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return String.format("%03d: %-40s %-20s %-20s %-5d total: %3d",
				codigo, titulo, autor, editora, ano, total);
	}
	
	public boolean emprestar() {
        if(total > 0) {
            total--;
            return true;
        }
        return false;
    }
	
	public boolean devolver() {
        total++;
        return true;
    }

	@Override
	public int compareTo(Livro outro) {
		return titulo.compareTo(outro.titulo);	
		//return outro.titulo.compareTo(titulo);	
	}
}
